import React from 'react';

const Footer = ({date}) => (
    <footer>
        <p>Todos los derechos reservados &copy; {date}</p>
    </footer>
);

//no es necesario el return

export default Footer;