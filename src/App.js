import React, { Fragment, useState } from 'react';
import Header from './components/Header';
import Footer from './components/Footer.js';
import Producto from './components/Producto.js';
import Carrito from './components/Carrito';

function App() {

  //crear listado de productos, esto es un state
  const [productos, setProducts] = useState(
    [
      { id: 1, nombre: 'Camisa ReactJS', precio: 50},
      { id: 2, nombre: 'Camisa VueJS', precio: 40},
      { id: 3, nombre: 'Camisa Node.js', precio: 30},
      { id: 4, nombre: 'Camisa Angular', precio: 20}
    ]
  );

  //state para el carrito de compras
  const [ carrito, agregarProducto ] = useState([]);



  const date = new Date().getFullYear();

  return (
    <Fragment>
      <Header titulo = 'Virtual Store'/> 

      <h1>Lista de Productos</h1>
      {productos.map(producto => (
        <Producto 
          key = {producto.id}
          producto = {producto}
          productos = {productos}
          carrito = {carrito}
          agregarProducto= {agregarProducto}
        />
      ))}

      <Carrito 
        carrito = {carrito}
        agregarProducto = {agregarProducto}
      />

      <Footer date = {date}/>
    </Fragment>
      
    //se pueden pasar mas variables a los components
  );
}

export default App;
